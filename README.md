# Auth with MetaMask wallet

<img src="https://gitlab.com/pheix-research/auth-with-metamask/-/raw/main/assets/auth-with-metamask.gif?v2" width="100%">

---

Discussion: https://gitlab.com/pheix-pool/core-perl6/-/issues/182

## Implementation in Pheix

Ticket: https://gitlab.com/pheix/dcms-raku/-/issues/186

Got it worked in MetaMask mobile inside built-in [WEB3.0 browser](https://support.metamask.io/ru/getting-started/how-to-use-the-metamask-mobile-browser/):

![Authenticate-on-Pheix-with-MetaMask](https://gitlab.com/-/project/5110501/uploads/d389a778b76fefdfa2d9ccb58e454758/Authenticate-on-Pheix-with-MetaMask.mp4){width=60%}

## Credits

Inspired by:

* Original article: https://dev.to/inancakduvan/connect-metamask-wallet-javascript-h1f
* Sources: https://github.com/inancakduvan/connect-metamask-wallet

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
