const connectButton = document.getElementById("connectButton");
const walletID = document.getElementById("walletID");
const reloadButton = document.getElementById("reloadButton");
const installAlert = document.getElementById("installAlert");
const mobileDeviceWarning = document.getElementById("mobileDeviceWarning");
const deployContract = document.getElementById("deployContract");
const deployButton = document.getElementById("deployButton");
const deploymentPkey = document.getElementById("deploymentPkey");

let account = undefined;

const startLoading = (obj) => {
    obj.classList.add("loadingButton");
};

const stopLoading = (obj) => {
    const timeout = setTimeout(() => {
        obj.classList.remove("loadingButton");
        clearTimeout(timeout);
    }, 300);
};

const isMobile = () => {
    let check = false;

    (function(a) {
        if (
            /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(
                a
            ) ||
            /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
                a.substr(0, 4)
            )
        )
            check = true;
    })(navigator.userAgent || navigator.vendor || window.opera);

    return check;
};

connectButton.addEventListener("click", () => {
    if (typeof window.ethereum !== "undefined") {
        startLoading(connectButton);

        ethereum
            .request({
                method: "eth_requestAccounts"
            })
            .then((accounts) => {
                account = accounts[0];

                walletID.innerHTML = `Wallet connected: <span>${account}</span>`;

                stopLoading(connectButton);

                deployContract.classList.remove("hide");
            })
            .catch((error) => {
                console.log(error, error.code);

                alert(error.code);
                stopLoading(connectButton);
            });
    } else {
        if (isMobile()) {
            mobileDeviceWarning.classList.add("show");
        } else {
            window.open("https://metamask.io/download/", "_blank");
            installAlert.classList.add("show");
        }
    }
});

deployButton.addEventListener("click", () => {
    if (typeof window.ethereum !== "undefined" && typeof account !== "undefined") {
        startLoading(deployButton);

        ethereum
            .request({
                "method": "eth_sendTransaction",
                "params": [{
                    "from": account,
                    "gas": "0x7a1200",
                    "data": "608060405234801561001057600080fd5b5060405161070738038061070783398101604081905261002f91610215565b600480546001600160a01b0319163317905542600355801580159061005957506402540be4008111155b610068576402540be40061006a565b805b600255811580159061007e575061012c8211155b61008a5761012c61008c565b815b6001558261009b57600561009d565b825b6000556100a8610107565b6040516020016100ba91815260200190565b60408051601f198184030181529082905280516020909101206005556001907f1afba253d16484eec92f8931ecedf989ad94e32e0a097a8b37c174c1c86524d490600090a25050506102c9565b6040516001600160601b03193360601b16602082015260009042908290439083906034016040516020818303038152906040528051906020012060001c61014e9190610259565b6040516001600160601b03194160601b166020820152459085906034016040516020818303038152906040528051906020012060001c61018e9190610259565b610198448761027b565b6101a2919061027b565b6101ac919061027b565b6101b6919061027b565b6101c0919061027b565b6040516020016101d291815260200190565b60408051601f1981840301815291905280516020909101206002549091506101fa8183610259565b6102049190610293565b61020e90826102b2565b9250505090565b60008060006060848603121561022a57600080fd5b8351925060208401519150604084015190509250925092565b634e487b7160e01b600052601160045260246000fd5b60008261027657634e487b7160e01b600052601260045260246000fd5b500490565b6000821982111561028e5761028e610243565b500190565b60008160001904831182151516156102ad576102ad610243565b500290565b6000828210156102c4576102c4610243565b500390565b61042f806102d86000396000f3fe608060405234801561001057600080fd5b506004361061004c5760003560e01c80630b1bcee7146100515780631d8557d71461006c5780637d0c338b14610084578063b2fd66751461008c575b600080fd5b610059610094565b6040519081526020015b60405180910390f35b6100746100de565b6040519015158152602001610063565b610074610195565b6100596101c0565b6004546000906001600160a01b031633146100b1576100b161031e565b6000546001546100c1919061034a565b6003546100ce9042610362565b6100d89190610379565b90505b90565b6004546000906001600160a01b031633146100fb576100fb61031e565b60005460015461010b919061034a565b6003546101189042610362565b116101855742600355610129610206565b60405160200161013b91815260200190565b60408051601f198184030181529082905280516020909101206005556002907f1afba253d16484eec92f8931ecedf989ad94e32e0a097a8b37c174c1c86524d490600090a261018f565b61018d610195565b505b50600190565b6004546000906001600160a01b031633146101b2576101b261031e565b6004546001600160a01b0316ff5b6004546000906001600160a01b031633146101dd576101dd61031e565b6000546001546101ed919061034a565b6003546101fa9042610362565b116100db575060055490565b6040516bffffffffffffffffffffffff193360601b16602082015260009042908290439083906034016040516020818303038152906040528051906020012060001c61025291906103b8565b6040516bffffffffffffffffffffffff194160601b166020820152459085906034016040516020818303038152906040528051906020012060001c61029791906103b8565b6102a1448761034a565b6102ab919061034a565b6102b5919061034a565b6102bf919061034a565b6102c9919061034a565b6040516020016102db91815260200190565b60408051601f19818403018152919052805160209091012060025490915061030381836103b8565b61030d91906103da565b6103179082610362565b9250505090565b634e487b7160e01b600052600160045260246000fd5b634e487b7160e01b600052601160045260246000fd5b6000821982111561035d5761035d610334565b500190565b60008282101561037457610374610334565b500390565b60008083128015600160ff1b85018412161561039757610397610334565b6001600160ff1b03840183138116156103b2576103b2610334565b50500390565b6000826103d557634e487b7160e01b600052601260045260246000fd5b500490565b60008160001904831182151516156103f4576103f4610334565b50029056fea2646970667358221220b23f2fb40548ce219ad099c5dbdbfd044347f36dc0cca45e01b6112af89cecbc64736f6c634300080c0033000000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000025800000000000000000000000000000000000000000000000000000000001e8098"
                }]
            })
            .then((TransactionHash) => {

                checkTx(TransactionHash);
            })
            .catch((error) => {
                console.log(error, error.code);

                alert(error.code);
                stopLoading(deployButton);
            });
    } else {
        if (isMobile()) {
            mobileDeviceWarning.classList.add("show");
        } else {
            window.open("https://metamask.io/download/", "_blank");
            installAlert.classList.add("show");
        }
    }
});

reloadButton.addEventListener("click", () => {
    window.location.reload();
});

function checkTx(TransactionHash) {
    console.log("Waiting for tx " + TransactionHash)

    deploymentPkey.innerHTML = "waiting for transaction...";

    if (typeof window.ethereum !== "undefined" && typeof account !== "undefined") {
        let interval = setInterval(() => {
            ethereum
                .request({
                    "method": "eth_getTransactionReceipt",
                    "params": [TransactionHash]
                })
                .then((receipt) => {
                    if (receipt) {

                        if (receipt.status === "0x1") {
                            console.log(receipt.contractAddress);
                            console.log(account);
                            console.log("0xb2fd6675");

                            ethereum
                                .request({
                                    "method": "eth_call",
                                    "params": [{
                                            "accessList": [],
                                            "to": receipt.contractAddress,
                                            "from": account,
                                            "input": "0xb2fd6675"
                                        },
                                        "latest"
                                    ]
                                })
                                .then((pKey) => {
                                    deploymentPkey.innerHTML = `pKey: <b>${pKey}</b>`;
                                })
                                .catch((error) => {
                                    deploymentPkey.innerHTML = "error while loading pKey";

                                    console.log(error, error.code);
                                });

                            stopLoading(deployButton);
                            deployButton.classList.add("hide");
                        } else {
                            console.log("transaction failed");

                            deploymentPkey.innerHTML = `transaction failed`;
                        }

                        clearInterval(interval);
                    }
                })
                .catch((error) => {
                    deploymentPkey.innerHTML = `error waiting for transaction ${TransactionHash}`;

                    console.log(error, error.code);

                    clearInterval(interval);
                    stopLoading(deployButton);
                });
        }, 1000)
    }
}
